import java.util.Scanner;

public class ejer_01 {
    public static void main(String[] args) {
        Scanner lectura = new Scanner(System.in);

        
        System.out.print("Ingrese un número entero positivo: ");
        int n = lectura.nextInt();
        int suma = 0;
        for (int i = 1; i <= n; i++) {
            if (i % 2 != 0) {
                suma += i;
            }
        }
        System.out.println("La suma de los " + n + " números impares es: " + suma);
    }
}