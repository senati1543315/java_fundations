import java.util.Scanner;

public class  ejer_03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("ingrese numero entero: ");
        int n = sc.nextInt();
        System.out.println("Los números primos desde 2 hasta " + n + " son:");
        for (int i = 2; i <= n; i++) {if (esPrimo(i)) {
                System.out.print(i + " ");
            }}}
    public static boolean esPrimo(int n) {if (n <= 1) {return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }}