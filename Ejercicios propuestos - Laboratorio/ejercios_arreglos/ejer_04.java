import java.util.Scanner;

public class ejer_04 {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    System.out.print("Ingrese la cantidad de números: ");
    int n = input.nextInt();

    int[] numeros = new int[n];
    for (int i = 0; i < n; i++) {
      System.out.print("Ingrese el número " + (i+1) + ": ");
      numeros[i] = input.nextInt();
    }

    int mcd = numeros;
    int i = 1;
    while (i < n && mcd != 1) {
      int num = numeros[i];
      while (num != 0) {
        int temp = num;
        num = mcd % num;
        mcd = temp;
      }
      i++;
    }

    System.out.println("El MCD de los números ingresados es: " + mcd);
  }
}