import java.util.Scanner;

public class ejercicioe20 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Leer los números ingresados por el usuario
        System.out.print("Ingrese el primer número entero: ");
        int numero1 = scanner.nextInt();

        System.out.print("Ingrese el segundo número entero: ");
        int numero2 = scanner.nextInt();

        // Calcular el MCD y el MCM
        int mcd = calcularMCD(numero1, numero2);
        int mcm = calcularMCM(numero1, numero2);

        // Imprimir el resultado
        System.out.println("El Máximo Común Divisor (MCD) de " + numero1 + " y " + numero2 + " es: " + mcd);
        System.out.println("El Mínimo Común Múltiplo (MCM) de " + numero1 + " y " + numero2 + " es: " + mcm);
    }

    // Función para calcular el Máximo Común Divisor (MCD)
    public static int calcularMCD(int numero1, int numero2) {
        while (numero2 != 0) {
            int temp = numero2;
            numero2 = numero1 % numero2;
            numero1 = temp;
        }
        return numero1;
    }

    // Función para calcular el Mínimo Común Múltiplo (MCM)
    public static int calcularMCM(int numero1, int numero2) {
        int mcm = (numero1 * numero2) / calcularMCD(numero1, numero2);
        return mcm;
    }
}
