import java.util.Scanner;

public class ejercicioe15 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Leer el número ingresado por el usuario
        System.out.print("Ingrese un número entero: ");
        int numero = scanner.nextInt();

        // Verificar si el número es primo
        boolean esPrimo = true;
        for (int i = 2; i < numero; i++) {
            if (numero % i == 0) {
                esPrimo = false;
                break;
            }
        }

        // Imprimir el resultado
        if (esPrimo) {
            System.out.println(numero + " es un número primo.");
        } else {
            System.out.println(numero + " no es un número primo.");
        }
    }
}