
import java.util.Scanner;

public class ejercicioe16 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Leer el peso y la altura ingresados por el usuario
        System.out.print("Ingrese su peso en kilogramos: ");
        double peso = scanner.nextDouble();

        System.out.print("Ingrese su altura en metros: ");
        double altura = scanner.nextDouble();

        // Calcular el IMC
        double imc = peso / (altura * altura);

        // Imprimir el resultado
        System.out.println("Su índice de masa corporal (IMC) es: " + imc);
    }
}