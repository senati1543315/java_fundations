import java.util.Scanner;

public class ejercicioe17 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Leer el número ingresado por el usuario
        System.out.print("Ingrese un número entero: ");
        int numero = scanner.nextInt();

        // Verificar si el número es positivo, negativo o cero
        if (numero > 0) {
            System.out.println(numero + " es un número positivo.");
        } else if (numero < 0) {
            System.out.println(numero + " es un número negativo.");
        } else {
            System.out.println("El número es cero.");
        }
    }
}