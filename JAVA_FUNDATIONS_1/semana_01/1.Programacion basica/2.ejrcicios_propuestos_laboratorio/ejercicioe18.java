import java.util.Scanner;

public class ejercicioe18 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Leer el número ingresado por el usuario
        System.out.print("Ingrese un número entero: ");
        int numero = scanner.nextInt();

        // Calcular el factorial
        int factorial = 1;
        for (int i = 1; i <= numero; i++) {
            factorial *= i;
        }

        // Imprimir el resultado
        System.out.println("El factorial de " + numero + " es: " + factorial);
    }
}
