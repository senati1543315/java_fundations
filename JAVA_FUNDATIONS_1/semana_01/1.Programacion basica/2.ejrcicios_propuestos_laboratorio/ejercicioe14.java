import java.util.Scanner;

public class ejercicioe14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Leer la edad ingresada por el usuario
        System.out.print("Ingrese su edad: ");
        int edad = scanner.nextInt();

        // Verificar si la edad es mayor o igual a 18
        if (edad >= 18) {
            System.out.println("Usted es mayor de edad.");
        } else {
            System.out.println("Usted es menor de edad.");
        }
    }
}