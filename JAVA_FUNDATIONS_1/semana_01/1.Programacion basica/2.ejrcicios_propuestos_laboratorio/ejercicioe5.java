import java.util.Scanner;

public class ejercicioe5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el primer número: ");
        int num1 = scanner.nextInt();
        System.out.print("Ingrese el segundo número: ");
        int num2 = scanner.nextInt();
        int diferencia = num1 - num2;
        System.out.println("La diferencia de los números es: " + diferencia);
    }
}