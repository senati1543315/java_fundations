import java.util.Scanner;

public class ejercicioe13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Leer la cadena de texto ingresada por el usuario
        System.out.print("Ingrese una cadena de texto: ");
        String cadena = scanner.nextLine();

        // Imprimir la cadena de texto en la consola
        System.out.println("La cadena de texto ingresada es: " + cadena);
    }
}