import java.util.Scanner;

public class ejercicioe19 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Leer el número ingresado por el usuario
        System.out.print("Ingrese un número entero: ");
        int numero = scanner.nextInt();

        // Calcular la suma de los números enteros desde 1 hasta el número ingresado
        int suma = 0;
        for (int i = 1; i <= numero; i++) {
            suma += i;
        }

        // Imprimir el resultado
        System.out.println("La suma de los números enteros desde 1 hasta " + numero + " es: " + suma);
    }
}