import java.util.TimeZone;

public class ejercicio16casa {
    public static void main(String[] args) {
        // Crear un objeto TimeZone con la zona horaria deseada
        TimeZone zonaHoraria = TimeZone.getTimeZone("America/New_York");

        // Imprimir la información de la zona horaria
        System.out.println("La zona horaria es: " + zonaHoraria.getID());
        System.out
                .println("El desplazamiento de la zona horaria es: " + zonaHoraria.getRawOffset() / 3600000 + " horas");
    }
}