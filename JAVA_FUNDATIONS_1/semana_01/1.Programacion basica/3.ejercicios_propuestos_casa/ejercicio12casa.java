import java.text.DateFormat;
import java.util.Date;

public class ejercicio12casa {
    public static void main(String[] args) {
        Date fechaActual = new Date();
        DateFormat formatoFecha = DateFormat.getDateInstance(DateFormat.SHORT);
        String fechaFormateada = formatoFecha.format(fechaActual);
        System.out.println("La fecha actual es: " + fechaFormateada);
    }
}