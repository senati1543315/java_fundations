public class ejercicio9casa {
    public static void main(String[] args) {
        // Ejemplo de uso de algunos métodos y constantes de la clase Math
        double numero1 = 8.5;
        double numero2 = 3.2;

        double suma = Math.addExact((int) numero1, (int) numero2);
        double resta = Math.subtractExact((int) numero1, (int) numero2);
        double multiplicacion = Math.multiplyExact((int) numero1, (int) numero2);
        double division = numero1 / numero2;
        double potencia = Math.pow(numero1, 2);
        double raizCuadrada = Math.sqrt(numero2);
        double valorAbsoluto = Math.abs(-numero1);

        // Imprimir los resultados
        System.out.println("La suma de " + (int) numero1 + " y " + (int) numero2 + " es: " + suma);
        System.out.println("La resta de " + (int) numero1 + " y " + (int) numero2 + " es: " + resta);
        System.out.println("La multiplicación de " + (int) numero1 + " y " + (int) numero2 + " es: " + multiplicacion);
        System.out.println("La división de " + numero1 + " y " + numero2 + " es: " + division);
        System.out.println("El cuadrado de " + numero1 + " es: " + potencia);
        System.out.println("La raíz cuadrada de " + numero2 + " es: " + raizCuadrada);
        System.out.println("El valor absoluto de " + (-numero1) + " es: " + valorAbsoluto);
    }
}