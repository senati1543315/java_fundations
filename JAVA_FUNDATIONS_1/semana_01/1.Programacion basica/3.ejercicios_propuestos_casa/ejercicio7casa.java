import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ejercicio7casa {
    public static void main(String[] args) {
        try {
            BufferedReader lector = new BufferedReader(new FileReader("ruta/archivo.txt"));
            String linea = lector.readLine();
            while (linea != null) {
                System.out.println(linea);
                linea = lector.readLine();
            }
            lector.close();
        } catch (IOException e) {
            System.out.println("Error al leer el archivo: " + e.getMessage());
        }
    }
}