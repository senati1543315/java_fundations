import java.util.Scanner;

public class ejercicio12e {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el primer número: ");
        int num1 = scanner.nextInt();
        System.out.print("Ingrese el segundo número: ");
        int num2 = scanner.nextInt();
        if (num1 == num2) {
            System.out.println("Los dos números son iguales");
        } else {
            System.out.println("Los dos números son diferentes");
        }
    }
}