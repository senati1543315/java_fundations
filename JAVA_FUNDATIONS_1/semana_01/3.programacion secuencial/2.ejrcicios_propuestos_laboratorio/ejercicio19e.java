import java.util.Scanner;

public class ejercicio19e {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Leer el número entero
        System.out.print("Ingrese un número entero: ");
        int numero = scanner.nextInt();

        // Verificar si el número es armónico
        double suma = 0.0;
        for (int i = 1; i <= numero; i++) {
            suma += 1.0 / i;
        }

        if (suma == numero) {
            System.out.println(numero + " es un número armónico.");
        } else {
            System.out.println(numero + " no es un número armónico.");
        }
    }
}
