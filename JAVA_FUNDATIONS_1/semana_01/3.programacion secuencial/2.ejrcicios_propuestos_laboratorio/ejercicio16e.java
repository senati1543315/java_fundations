import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ejercicio16e {
    public static void main(String[] args) {
        List<String> lista = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        // Leer la lista de palabras
        System.out.print("Ingrese la lista de palabras separadas por espacios: ");
        String[] palabras = scanner.nextLine().split(" ");
        for (String palabra : palabras) {
            lista.add(palabra);
        }

        // Leer la letra determinada
        System.out.print("Ingrese la letra determinada: ");
        String letra = scanner.nextLine();

        // Verificar cuántas palabras contienen la letra determinada
        int count = 0;
        for (String palabra : lista) {
            if (palabra.contains(letra)) {
                count++;
            }
        }

        System.out.println("Hay " + count + " palabras que contienen la letra " + letra + ".");
    }
}