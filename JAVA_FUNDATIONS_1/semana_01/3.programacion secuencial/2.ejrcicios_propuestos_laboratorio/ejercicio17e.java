import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ejercicio17e {
    public static void main(String[] args) {
        List<String> lista = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        // Leer la lista de palabras
        System.out.print("Ingrese la lista de palabras separadas por espacios: ");
        String[] palabras = scanner.nextLine().split(" ");
        for (String palabra : palabras) {
            lista.add(palabra);
        }

        // Leer el número determinado
        System.out.print("Ingrese el número determinado: ");
        int numero = scanner.nextInt();

        // Verificar cuántas palabras tienen una longitud mayor que el número
        // determinado
        int count = 0;
        for (String palabra : lista) {
            if (palabra.length() > numero) {
                count++;
            }
        }

        System.out.println("Hay " + count + " palabras que tienen una longitud mayor que " + numero + ".");
    }
}