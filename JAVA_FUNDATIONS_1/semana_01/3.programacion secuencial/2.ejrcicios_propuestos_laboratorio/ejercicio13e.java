import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ejercicio13e {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        // Leer la lista de números enteros
        System.out.print("Ingrese la lista de números enteros separados por espacios: ");
        String[] numeros = scanner.nextLine().split(" ");
        for (String numero : numeros) {
            lista.add(Integer.parseInt(numero));
        }

        List<Integer> fibonacci = new ArrayList<>();
        fibonacci.add(0);
        fibonacci.add(1);

        // Generar la sucesión de Fibonacci hasta el número más grande de la lista
        int n = 2;
        while (fibonacci.get(n - 1) < lista.get(lista.size() - 1)) {
            fibonacci.add(fibonacci.get(n - 1) + fibonacci.get(n - 2));
            n++;
        }

        // Verificar si cada número de la lista está en la sucesión de Fibonacci
        int count = 0;
        for (int i : lista) {
            if (fibonacci.contains(i)) {
                count++;
            }
        }

        System.out.println("Hay " + count + " numeros de Fibonacci en la lista.");
    }
}