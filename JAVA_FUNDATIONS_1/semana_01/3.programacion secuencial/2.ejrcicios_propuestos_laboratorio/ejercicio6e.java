import java.util.Scanner;

public class ejercicio6e {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el primer número: ");
        int num1 = scanner.nextInt();
        System.out.print("Ingrese el segundo número: ");
        int num2 = scanner.nextInt();
        int cociente = num1 / num2;
        int resto = num1 % num2;
        System.out.println("El cociente de los números es: " + cociente);
        System.out.println("El resto de los números es: " + resto);
    }
}