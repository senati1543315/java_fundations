import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ejercicio20e {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        // Leer la lista de números enteros
        System.out.print("Ingrese la lista de números enteros separados por espacios: ");
        String[] numeros = scanner.nextLine().split(" ");
        for (String numero : numeros) {
            lista.add(Integer.parseInt(numero));
        }

        // Leer el número determinado
        System.out.print("Ingrese el número determinado: ");
        int numeroDivisores = scanner.nextInt();

        // Verificar cuántos números tienen una cantidad de divisores mayor que el
        // número determinado
        int count = 0;
        for (int numero : lista) {
            int cantidadDivisores = 0;
            for (int i = 1; i <= numero; i++) {
                if (numero % i == 0) {
                    cantidadDivisores++;
                }
            }
            if (cantidadDivisores > numeroDivisores) {
                count++;
            }
        }

        System.out.println(
                "Hay " + count + " números que tienen una cantidad de divisores mayor que " + numeroDivisores + ".");
    }
}