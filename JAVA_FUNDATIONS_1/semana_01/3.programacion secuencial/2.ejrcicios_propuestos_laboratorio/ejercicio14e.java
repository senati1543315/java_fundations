import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ejercicio14e {
    public static void main(String[] args) {
        List<String> lista = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        // Leer la lista de palabras
        System.out.print("Ingrese la lista de palabras separadas por espacios: ");
        String[] palabras = scanner.nextLine().split(" ");
        for (String palabra : palabras) {
            lista.add(palabra);
        }

        // Verificar si cada palabra es un palíndromo
        int count = 0;
        for (String palabra : lista) {
            if (esPalindromo(palabra)) {
                count++;
            }
        }

        System.out.println("Hay " + count + " palindromos en la lista.");
    }

    public static boolean esPalindromo(String palabra) {
        int i = 0;
        int j = palabra.length() - 1;

        while (i < j) {
            if (palabra.charAt(i) != palabra.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }

        return true;
    }
}