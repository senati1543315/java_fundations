import java.util.regex.Pattern;

public class ejercicio10casa {
    public static void main(String[] args) {
        String patron = "\\d+";
        Pattern pattern = Pattern.compile(patron);
        String texto = "La edad de Juan es 25";
        boolean coincide = pattern.matcher(texto).find();
        System.out.println("¿El texto contiene un número? " + coincide);
    }
}