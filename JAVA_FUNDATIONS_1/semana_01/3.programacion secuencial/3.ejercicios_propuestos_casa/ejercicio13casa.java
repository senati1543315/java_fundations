import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ejercicio13casa {
    public static void main(String[] args) {
        List<String> lista = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        // Leer la lista de palabras
        System.out.print("Ingrese la lista de palabras separadas por espacios: ");
        String[] palabras = scanner.nextLine().split(" ");
        for (String palabra : palabras) {
            lista.add(palabra);
        }

        // Verificar cuántas palabras tienen una letra que aparece más de dos veces pero
        // menos de cinco veces
        int count = 0;
        for (String palabra : lista) {
            boolean encontrado = false;
            for (int i = 0; i < palabra.length(); i++) {
                char letra = palabra.charAt(i);
                int cantidad = 0;
                for (int j = 0; j < palabra.length(); j++) {
                    if (palabra.charAt(j) == letra) {
                        cantidad++;
                    }
                }
                if (cantidad > 2 && cantidad < 5) {
                    encontrado = true;
                    break;
                }
            }
            if (encontrado) {
                count++;
            }
        }

        System.out.println("Hay " + count
                + " palabras que tienen una letra que aparece más de dos veces pero menos de cinco veces.");
    }
}